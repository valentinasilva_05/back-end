var express= require('express');
var app = express();
var bodyParser=require('body-parser');
var usersFile = require('./users.json');
var accountsFile = require('./accounts.json');
var port=process.env.PORT || 3000;
const URI='/api-uruguay/v1/';

app.use(bodyParser.json());

// GET users
app.get (URI +'users',
      function (req,res) {
        res.send(usersFile);
      }
);

// GET user con ID
app.get (URI +'users/:idUser',
      function (req,res) {

        let idUser= req.params.idUser;
        res.send(usersFile[idUser-1]);
      }
);

// GET accounts
app.get (URI +'accounts',
      function (req,res) {
        res.send(accountsFile);
      }
);

// GET accounts con ID
app.get (URI +'accounts/:idCuenta',
      function (req,res) {

        let idCuenta= req.params.idCuenta;
        res.send(accountsFile[idCuenta-1]);
      }
);

// POST users
app.post(URI + 'users',
  function (req,res) {
    console.log('POST users');
    console.log(req.headers);
    let tam=usersFile.length;

    var userNuevo= req.headers;
    userNuevo.id=tam;
    usersFile.push(userNuevo);
    res.send({'msg': 'Nuevo usuario agregado correctamente',userNuevo});
  }
);

// PUT users
app.put(URI + 'users/:id', function(req, res){
 console.log('put users');
 let idUpdate = req.params.id;
 let existe = true;
 if(usersFile[idUpdate-1] == undefined){
   console.log('Usuario NO existe');
   existe = false;
 }
 else {
   var userNuevo = {
     'id': idUpdate,
     'first_name': req.body.first_name,
     'last_name': req.body.last_name,
     'email': req.body.email,
     'password': req.body.password
   };
   usersFile[idUpdate - 1] = userNuevo;
 }
 var mensRespone = existe ? {'msg':'update ok'} : {'msg':'failed'};
 //if(idUpdate == usersFile[idUpdate-1].id)
 var statusCode = existe ? 200 : 400;
 res.status(statusCode).send(mensRespone);
});

//DELETE user
app.delete(URI + 'users/:id', function(req, res){
 console.log('delete users');
 let idDelete = req.params.id;
 let existe = true;
 if(usersFile[idDelete-1] == undefined){
   console.log('Usuario NO existe');
   existe = false;
 }
 else {
   usersFile.splice(idDelete - 1, 1);
 }

 var mensRespone = existe ? {'msg':'delete ok'} : {'msg':'delete failed'};
 //if(idUpdate == usersFile[idUpdate-1].id)
 var statusCode = existe ? 200 : 400;
 res.status(statusCode).send(mensRespone);
});

// GET con query string
app.get (URI +"users2",
      function (req,res) {
        console.log('GET con query string');
        let qname= req.query.qname;
        let lname= req.query.last_name;
        var statuscode=200;
        var usuario= usersFile.filter(user=>user.first_name==qname && user.last_name==lname);
        if(usuario.length>0){
          res.send(statuscode,{'msg': 'usuario encontrado',usuario});
        }else{
          statuscode=400;
            res.send(statuscode,{'msg': 'usuario NO encontrado'});
        }

      }
);

app.listen(port);
console.log('Escuchando en el puerto 3000...');
