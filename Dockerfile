# Imagen docker base
FROM node:latest

# Definimos directorio de trabajo de docker
WORKDIR /docker-api

#Copiar archivos del proyecto
ADD . /docker-api

# Instala las dependencias del proyecto
#RUN npm install

#Puerto donde exponemos contenedor
EXPOSE 3000

#Comando para lanzar la app
CMD ["npm", "start"]
