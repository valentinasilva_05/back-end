var express= require('express');
var app = express();
var bodyParser=require('body-parser');
var requestJson = require('request-json');
var Retorno = require('./models/retorno.js');
var cors= require('cors');

// Leo las config
let config = require('./config/config.js');
var URI= config.URI;
var port=process.env.PORT || config.port;

app.use(bodyParser.json());
app.use(cors());
app.listen(port);

//Controladores del api
var JWTexpirados = require('./models/JWTexpirados.js');
var jwt = require('jsonwebtoken');
var controllerMiddleware = require('./controllers/middleware.js');
var controllerLogin= require('./controllers/login.js');
var controllerAccounts= require('./controllers/accounts.js');
var controllerTransactions= require('./controllers/transactions.js');
var controllerUsers= require('./controllers/users.js');

//middleware
app.use(controllerMiddleware.middleware);

//login
app.post(URI + 'login',controllerLogin.post);
app.post(URI + 'logout',controllerLogin.logout);

//accounts
app.get(URI + 'accounts/:idUser', controllerAccounts.get);

//transactions
app.get(URI +'transactions/:idAccount', controllerTransactions.get);
app.post(URI + 'transactions', controllerTransactions.post);

//users
app.get(URI +'users', controllerUsers.get);
app.post(URI + 'users', controllerUsers.post);
app.put(URI + 'users/:id', controllerUsers.put);
app.delete(URI + 'users/:id', controllerUsers.delete);

console.log('Escuchando en el puerto '+port);
