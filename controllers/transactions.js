var requestJson = require('request-json');
var Retorno = require('../models/retorno.js');
var moment = require('moment');

// Leo las configuraciones
let config = require('../config/config.js');
var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var clienteMlab;
var URI= config.URI;

// GET transaction (movimientos) de una cuenta consumiendo API REST de MLAB
// Obtiene los movimientos asociados a la cuenta
function get(req, res) {
    console.log('Entra al get de movimientos');
    var retorno= new Retorno();
    var id = req.params.idAccount;
    var queryFiltro = 'q={"id_account":' + id + '}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/transaction?s={"date":1}&'+ queryFiltro  +"&" + apiKey);
     clienteMlab.get('', function(err, resM, body) {
         if (!err) {
           if (body.length > 0){
             res.status(200);
             retorno.setCodigo(0);
             retorno.setDescripcionError('');
             body.sort(custom_sort);
             retorno.setData(body);
             res.send(retorno);
           }else {
             res.status(404);
             retorno.setCodigo(404);
             retorno.setDescripcionError('No se encontraron transacciones para el usuario');
             res.send(retorno);
           }
         }else{
           res.status(500);
           retorno.setCodigo(500);
           retorno.setDescripcionError('Ocurrió un error obteniendo las transacciones');
           res.send(retorno);
         }
       });
}

// Función que compara fechas para el sort de los movimientos
function custom_sort(a, b) {
  var dateStrA = a.date.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
  var dateStrB = b.date.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3");
  return new Date(dateStrB) - new Date(dateStrA);
}

// POST transaction consumiendo API REST de MLAB
// Realiza una transferencia entre 2 cuentas, valiando que exista la cuenta destino
function post(req, res) {
  console.log('Entra al post de movimientos');
  var idAccount= req.body.id_account;
  var IBAN_origen= req.body.IBAN;
  var monto=req.body.amount;
  var datetime=  req.body.date;
  var retorno= new Retorno();
  var newID=0;

  // Obtengo la cuenta destino
  let filtroCuentaDestino =  `q={'IBAN' : '${req.body.other_account}'}`;
  let filtroCuentaOrigen = 'q={"id":' + idAccount + '}';
  clienteMlab = requestJson.createClient(urlMlabRaiz +  '/account?' +filtroCuentaDestino + "&" + apiKey+"&l=1");

  clienteMlab.get('', function(error, resM, body){
        if (body.length == 0) {
          res.status(404);
          retorno.setCodigo(404);
          retorno.setDescripcionError('La cuenta destino no existe');
          res.send(retorno);
          return;
        } else {
          // Actualizo saldo cuenta destino
          var saldoCuentaDestino= parseFloat(body[0].balance);
          var saldoNuevo= saldoCuentaDestino+parseFloat(monto);

          var updateCuentaDestino=body[0];
          updateCuentaDestino.balance=saldoNuevo;

          clienteMlab = requestJson.createClient(urlMlabRaiz + "/account");
          clienteMlab.put('?q={"id": ' + updateCuentaDestino.id + '}&' + apiKey,updateCuentaDestino ,
             function(errP, resP, bodyP) {
               if (!errP) {
                    //Obtengo el id de ultima transaccion y la creo en la cuenta destino
                    clienteMlab = requestJson.createClient(urlMlabRaiz +  '/transaction?' + apiKey);
                    clienteMlab.get('', function(error, resM, body){

                        newID = body.length + 1;
                        var newTransaction = {
                          "id_account" : updateCuentaDestino.id,
                          "id_transaction" : newID,
                          "date" : datetime,
                          "concept" : req.body.concept,
                          "type" : "CREDITO",
                          "other_account" : IBAN_origen,
                          "amount": monto
                        };

                        clienteMlab.post(urlMlabRaiz + "/transaction?" + apiKey, newTransaction,
                        function(error, resM, body){
                          if(!error){
                              //Creo la transaccion en la creo en la cuenta origen
                                newID =newID+ 1;

                                var newTransaction = {
                                  "id_account" : idAccount,
                                  "id_transaction" : newID,
                                  "date" : datetime,
                                  "concept" : req.body.concept,
                                  "type" : req.body.type,
                                  "other_account" : req.body.other_account,
                                  "amount": monto
                                };
                                  clienteMlab.post(urlMlabRaiz + "/transaction?" + apiKey, newTransaction,
                                    function(error, resM, body){
                                        if(!error){

                                            // Actualizo el saldo cuenta origen
                                            clienteMlab = requestJson.createClient(urlMlabRaiz +  '/account?' +filtroCuentaOrigen + "&" + apiKey);
                                            clienteMlab.get('', function(error, resM, body){
                                                if(!error){
                                                  if (body.length > 0) {
                                                    var saldo= parseFloat(body[0].balance);
                                                    var saldoNuevo= saldo-parseFloat(monto);

                                                    var updateCuenta=body[0];
                                                    updateCuenta.balance=saldoNuevo;

                                                    clienteMlab = requestJson.createClient(urlMlabRaiz + "/account");

                                                    clienteMlab.put('?q={"id": ' + updateCuenta.id + '}&' + apiKey,updateCuenta ,
                                                       function(errP, resP, bodyP) {
                                                         if (!errP) {
                                                           res.status(200);
                                                           retorno.setCodigo(0);
                                                           retorno.setDescripcionError('');
                                                           retorno.setData(bodyP);
                                                           res.send(retorno);
                                                         }else{
                                                           res.status(500);
                                                           retorno.setCodigo(500);
                                                           retorno.setDescripcionError('Error al actualizar saldo origen');
                                                           res.send(retorno);
                                                         }

                                                      });

                                                    }else{
                                                      res.status(404);
                                                      retorno.setCodigo(404);
                                                      retorno.setDescripcionError('No existe la cuenta origen');
                                                      res.send(retorno);
                                                    }
                                                  }else{
                                                  res.status(500);
                                                  retorno.setCodigo(500);
                                                  retorno.setDescripcionError('Error actualizando saldo origen');
                                                  res.send(retorno);
                                                  return;
                                                  }
                                              });
                                        }else{
                                          res.status(500);
                                          retorno.setCodigo(500);
                                          retorno.setDescripcionError('Error actualizando saldo origen');
                                          res.send(retorno);
                                          return;
                                        }
                                    });

                          }else{
                            res.status(500);
                            retorno.setCodigo(500);
                            retorno.setDescripcionError('Error realizando transaccion cuenta origen');
                            res.send(retorno);
                            return;
                          }
                      });
                        });
            }else{
              res.status(500);
              retorno.setCodigo(500);
              retorno.setDescripcionError('Error realizando transaccion cuenta destino');
              res.send(retorno);
              return;
            }
          });
        }
  });
}

module.exports.get=get;
module.exports.post=post;
