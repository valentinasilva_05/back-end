var requestJson = require('request-json');
var Retorno = require('../models/retorno.js');

// Leo las configuraciones
let config = require('../config/config.js');
var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var clienteMlab;
var queryBaseUser=config.queryBaseUser;
var URI= config.URI;

//GET USERS consumiendo API REST de MLAB
//Obtengo los usuarios
function get(req, res) {
    var retorno= new Retorno();
    clienteMlab = requestJson.createClient(urlMlabRaiz + queryBaseUser +"s={'id':-1}&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length > 0){
          res.status(200);
          retorno.setCodigo(0);
          retorno.setDescripcionError('');
          retorno.setData(body);
          res.send(retorno);
        }else {
          res.status(404);
          retorno.setCodigo(404);
          retorno.setDescripcionError('No existen usuarios');
          res.send(retorno);
        }
      }else{
        res.status(500);
        retorno.setCodigo(500);
        retorno.setDescripcionError('Error obteniendo usuaruiso');
        res.send(retorno);
      }
    });

}

// POST users consumiendo API REST de MLAB
// Doy de alta un usuario
function post(req, res){
  var retorno= new Retorno();
   let filtro = "&s={'id':-1}&l=1&";
   clienteMlab = requestJson.createClient(urlMlabRaiz +  queryBaseUser +filtro+ apiKey);
   clienteMlab.get('', function(error, resM, body){
       newID = body[0].id + 1;
       console.log("newID:" + newID);
       var newUser = {
         "id" : newID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       console.log(newUser);
       clienteMlab.post(urlMlabRaiz + "/user?" + apiKey, newUser,
         function(error, resM, body){
           res.status(200);
           retorno.setCodigo(0);
           retorno.setDescripcionError('');
           retorno.setData(body);
           res.send(retorno);
         });
     });
 }

//put USERS consumiendo API REST de MLAB
// Actualizo un usuario
function put(req, res){
 console.log('put users');
 var retorno= new Retorno();
 let id = req.params.id;

 var queryFiltro = 'q={"id":' + id + '}';
 clienteMlab = requestJson.createClient(urlMlabRaiz + queryBaseUser+ queryFiltro + "&l=1&" + apiKey);

 clienteMlab.get('', function(error, resM, body){

   if (!error) {
     if (body.length > 0){
       var usuarioUpdate=req.body;
       usuarioUpdate.id= body[0].id;
       clienteMlab = requestJson.createClient(urlMlabRaiz + "/user");

       clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey,usuarioUpdate ,
          function(errP, resP, bodyP) {
            if (!errP) {
              res.status(200);
              retorno.setCodigo(0);
              retorno.setDescripcionError('');
              retorno.setData(bodyP);
              res.send(retorno);
            }else{
              res.status(500);
              retorno.setCodigo(500);
              retorno.setDescripcionError('Error al actualizar usuario');
              res.send(retorno);
            }

         })

     }else {
       res.status(404);
       retorno.setCodigo(404);
       retorno.setDescripcionError('Usuario no encontrado');
       res.send(retorno);
     }
   }else{
     res.status(500);
     retorno.setCodigo(500);
     retorno.setDescripcionError('Error obteniendo usuario');
     res.send(retorno);
   }
 });
}

 //delete USERS consumiendo API REST de MLAB
 // Elimino un usuario
 function eliminar(req, res){
  console.log('delete users');

  var retorno= new Retorno();
  let id = req.params.id;

  var queryFiltro = 'q={"id":' + id + '}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?'+ queryFiltro + "&l=1&" + apiKey);

  clienteMlab.get('', function(error, resM, body){

    if (!error) {
      if (body.length > 0){
        var idUsuarioMongo=body[0]._id.$oid;
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/user/");
        console.log(idUsuarioMongo);
        clienteMlab.delete(idUsuarioMongo + '?' + apiKey ,
           function(errP, resP, bodyP) {
             if (!errP) {
               res.status(200);
               retorno.setCodigo(0);
               retorno.setDescripcionError('');
               retorno.setData(bodyP);
               res.send(retorno);
             }else{
               res.status(500);
               retorno.setCodigo(500);
               retorno.setDescripcionError('Error al eliminar usuario');
               res.send(retorno);
             }

          })

      }else {
        res.status(404);
        retorno.setCodigo(404);
        retorno.setDescripcionError('Usuario no encontrado');
        res.send(retorno);
      }
    }else{
      res.status(500);
      retorno.setCodigo(500);
      retorno.setDescripcionError('Error obteniendo usuario');
      res.send(retorno);
    }
  });

 }

module.exports.get=get;
module.exports.post=post;
module.exports.put=put;
module.exports.delete=eliminar;
