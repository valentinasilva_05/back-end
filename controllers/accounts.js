var requestJson = require('request-json');
var Retorno = require('../models/retorno.js');

// Leo las configuraciones
let config = require('../config/config.js');
var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var clienteMlab;
var URI= config.URI;

// GET accounts de un usuario consumiendo API REST de MLAB
// Obtiene las cuentas del usuario recibido por parametro, retornando error en caso contrario
function get (req,res) {
    console.log('Entra al get de cuentas ' );
    var id = req.params.idUser;
    var retorno= new Retorno();
    
    var queryFiltro = 'q={"id_user":' + id + '}';
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/account?'+ queryFiltro + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
         if (!err) {
           if (body.length > 0){
             res.status(200);
             retorno.setCodigo(0);
             retorno.setDescripcionError('');
             retorno.setData(body);
             res.send(retorno);
           }else {
             res.status(404);
             retorno.setCodigo(404);
             retorno.setDescripcionError('No se encontraron cuentas para el usuario');
             res.send(retorno);
           }
         }else{
           res.status(500);
           retorno.setCodigo(500);
           retorno.setDescripcionError('Ocurrió un error obteniendo las cuentas');
           res.send(retorno);
         }
       });
}

module.exports.get=get;
