var config = require('../config/config.js');
var Retorno = require('../models/retorno.js');
var jwt = require('jsonwebtoken');

const URI = config.URI;

/** Middleware que procesa todos los request/response**/
function middleware(req, res, next) {

  var retorno = new Retorno();

  // Verifico que venga el JWT y sea válido si es otro metodo distinto a login
  let pathLogin = URI + 'login';

  if( !( req.originalUrl == pathLogin)){
      // Obtengo el token jwt
      let token = req.get('authorization');

      if(token==undefined){
          res.status(401);
          retorno.setCodigo(401);
          retorno.setDescripcionError('El acceso al servicio no es autorizado');
          res.send(retorno);
      }else{
        try{
          //Verifico que no este expirado el token porque sea uno logueado
          let expirado = false
          for(let i = 0; i < JWTexpirados.length && expirado; i++){
            if(JWTexpirados[i] == token){
              expirado = true;
            }
          }
          if(!expirado){
            jwt.verify(token,config.claveJWT,function(err,decode){
              if(err){
                res.status(401);
                retorno.setCodigo(401);
                retorno.setDescripcionError('Error al validar el token de seguridad');
                res.send(retorno);
              }else{
                next();
              }
            });
          }else{
            res.status(401);
            retorno.setCodigo(false);
            retorno.setDescripcionError('El Token JWT de seguridad es inválido');
            res.send(retorno);
          }
        }catch(error){
          res.status(401);
          retorno.setCodigo(401);
          retorno.setDescripcionError('No se pudo validar token JWT, debe generar un nuevo token');
          res.send(retorno);
        }
      }
  }else{
    next();
  }
}

module.exports.middleware = middleware;
