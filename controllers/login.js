var jwt = require('jsonwebtoken');
var requestJson = require('request-json');
var JWTexpirados = require('../models/JWTexpirados.js');
var Retorno = require('../models/retorno.js');

// Leo las configuraciones
let config = require('../config/config.js');
var urlMlabRaiz = config.urlMlabRaiz;
var apiKey = config.apiKey;
var clienteMlab;
var queryBaseUser=config.queryBaseUser;
var URI= config.URI;

// Realiza el login del usuario y genera un token JWT para validar posteriores peticiones a la API
function post(req, res) {
    console.log('Entra al login' );
    var retorno= new Retorno();
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';

    clienteMlab = requestJson.createClient(urlMlabRaiz +queryBaseUser+queryStringEmail + apiKey );
    clienteMlab.get('',function(error, respuestaMLab , body) {

        if (!error) {
          var respuesta = body[0];

          if(respuesta != undefined){
              if (respuesta.password == pass) {

                var session = {"logged":true};
                var login = '{"$set":' + JSON.stringify(session) + '}';

                clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(login),
                  function(errorP, respuestaMLabP, bodyP) {
                    if(!errorP){

                      //Genero token JWT

                      var tokenData = {
                        "id" : respuesta.id,
                        "first_name" : respuesta.first_name,
                        "last_name" : respuesta.last_name,
                        "email" : respuesta.email
                     }

                     var token = jwt.sign(tokenData, config.claveJWT);

                     var resultado = {
                       "id" : respuesta.id,
                       "first_name" : respuesta.first_name,
                       "last_name" : respuesta.last_name,
                       "email" : respuesta.email,
                       "token" : token
                     };
                     res.status(200);
                     retorno.setCodigo(0);
                     retorno.setDescripcionError('');
                     retorno.setData(resultado);
                     res.send(retorno);
                    }else{
                      res.status(500);
                      retorno.setCodigo(500);
                      retorno.setDescripcionError('Ocurrió un error en el login');
                      res.send(retorno);
                    }

                  }
                );
              }
              else {
                res.status(401);
                retorno.setCodigo(401);
                retorno.setDescripcionError('El password ingresado no es correcto');
                res.send(retorno);
              }
          } else {
            res.status(404);
            retorno.setCodigo(404);
            retorno.setDescripcionError('No existe un usuario con ese email cargado');
            res.send(retorno);
          }

        }else{
          res.status(500);
          retorno.setCodigo(404);
          retorno.setDescripcionError('Ocurrió un error al realizar el login');
          res.send(retorno);
        }
      });
  }

// Realiza el logout del usuario y agrega el token a la lista de expirados para que no pueda ser utilizado
function logout(req, res) {
  var retorno= new Retorno();
  var token = req.get('authorization');
  JWTexpirados.push(token);

  var email = req.body.email;
  var queryStringEmail = 'q={"email":"' + email + '"}&';

  clienteMlab = requestJson.createClient(urlMlabRaiz +queryBaseUser+queryStringEmail + apiKey);
  clienteMlab.get('' ,function(error, respuestaMLab, body) {

    var respuesta = body[0];

    if(respuesta != undefined){
        var session = {"logged":true};
        var logout = '{"$unset":' + JSON.stringify(session) + '}';

        clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(logout),
          function(errorP, respuestaMLabP, bodyP) {
            if(!errorP){
              res.status(200);
              retorno.setCodigo(0);
              retorno.setData(body[0]);
              res.send(retorno);
            }else{
              res.status(500);
              retorno.setCodigo(500);
              retorno.setDescripcionError('Ocurrió un error en el logout');
              res.send(retorno);
            };
          });
      } else {
        res.status(500);
        retorno.setCodigo(500);
        retorno.setDescripcionError('Ocurrió un error al realizar el logout');
        res.send(retorno);
      }
  });
}

module.exports.post=post;
module.exports.logout=logout;
