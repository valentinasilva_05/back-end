/** Clase para encapsular las respuestas de la api */
class Retorno{

  setCodigo(codigo){
    this.codigo = codigo;
  }

  setDescripcionError(error){
    this.error = error;
  }

  setData(data){
    this.data = data;
  }

  getDescripcionError(){
    return this.error;
  }

  getCodigo(){
    return this.codigo;
  }

}

module.exports = Retorno;
