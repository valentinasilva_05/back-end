//Variables de MLAB
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techubduruguay/collections";
var apiKey = "apiKey=O8cEJpKLSdKXIdd6QgSeOA0mLUyspIVF";
var queryBaseUser="/user?f={'_id':0}&";

//puerto
const port=3000;

//URI de la api
const URI= "/api-uruguay/v1/";

//clave JWT
const claveJWT= "JWT_api_techu";

//Exporto las variables
module.exports.urlMlabRaiz = urlMlabRaiz;
module.exports.apiKey = apiKey;
module.exports.queryBaseUser = queryBaseUser;
module.exports.port=port;
module.exports.URI=URI;
module.exports.claveJWT=claveJWT;
